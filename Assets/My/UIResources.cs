﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIResources
{
    public static Object[] sprites;
    public static Object[] fonts;
    public static Object[] sounds;
    public static void loadResources()
    {
        if (sprites == null)
        {
            sprites = Resources.LoadAll("textures", typeof(Sprite));
        }
        if (fonts == null)
        {
            fonts = Resources.LoadAll("fonts", typeof(Font));
        }
        if (sounds == null)
        {
            sounds = Resources.LoadAll("sound", typeof(AudioClip));
            foreach (AudioClip t in sounds)
            {
                t.LoadAudioData();
            }
        }
    }

    public static Sprite getSprite(string name)
    {
        foreach (var t in sprites)
        {
            if (t.name == name)
            {
                return (Sprite)t;
            }
        }
        return null;
    }

    public static Font getFont(string name)
    {
        foreach (var t in fonts)
        {
            if (t.name == name)
            {
                return (Font)t;
            }
        }
        return null;
    }

    public static AudioClip getClip(string name)
    {
        foreach (var t in sounds)
        {
            if (t.name == name)
            {
                return (AudioClip)t;
            }
        }
        return null;
    }

    public static float getSpriteRatio(string name)
    {
        foreach (var t in sprites)
        {
            if (t.name == name)
            {
                return (((Sprite)t).rect.width / ((Sprite)t).rect.height);
            }
        }
        return 0;
    }
}
