﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using UnityEngine;

public class Misc
{
    private static System.Random random = new System.Random();
    public static string generateRandomID(int length)
    {
        string result = "";
        for (int count = 0; count < length; count++)
        {
            result += (char)random.Next(65, 91);
        }
        return result;
    }

    public static int getRandomBetween(int min, int max)
    {
        return random.Next(min, max+1);
    }
}
