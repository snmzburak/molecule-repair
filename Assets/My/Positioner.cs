﻿using System;
using UnityEngine;
using UnityEngine.UI;

class Positioner
{
    public static float getScaledYX()
    {
        return (float)Screen.height / (float)Screen.width;
    }

    public static float getScaledX(float x)
    {
        return (Screen.width * x) / 1000f;
    }

    public static float getScaledY(float y)
    {
        return (Screen.height * y) / 1000f;
    }

    public static float getPixelToX(float x)
    {
        return (x * 1000) / Screen.width;
    }

    public static float getPixelToY(float y)
    {
        return ((Screen.height - y) * 1000)/(Screen.height);
    }

    public static GameObject Image(GameObject canvas, Sprite resource, float width, float height, float left, float top)
    {
        float screenWidth = Screen.width;
        float screenHeight = Screen.height;

        float Width = (width * screenWidth) / 1000f;
        float Height = (height * screenHeight) / 1000f;
        float Left = (left * screenWidth) / 1000f;
        float Top = ((1000 - top - height) * screenHeight) / 1000f;

        GameObject newImage = new GameObject();
        newImage.AddComponent<Image>();
        newImage.transform.SetParent(canvas.transform);

        RectTransform objectRectTransform = newImage.GetComponent<RectTransform>();
        Vector2 position = Vector2.zero;
        position.x = Left+Width/2;
        position.y = Top+Height/2;
        objectRectTransform.transform.position = position;

        objectRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Width);
        objectRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Height);

        newImage.GetComponent<Image>().sprite = resource;

        return newImage;
    }


    public static GameObject Image(GameObject canvas, Sprite resource, float width, float height, float left, float top, Action<GameObject,int> action, int parameter)
    {
        GameObject newImage = Image(canvas, resource, width, height, left, top);
        newImage.AddComponent<UIClick>();
        newImage.GetComponent<UIClick>().addClickEvent(action, parameter);

        return newImage;
    }

    public static GameObject Image(GameObject canvas, Sprite resource, float width, float height, float left, float top, Action action)
    {
        GameObject newImage = Image(canvas, resource, width, height, left, top);
        newImage.AddComponent<UIClick>();
        newImage.GetComponent<UIClick>().addClickEvent(action);

        return newImage;
    }

    public static GameObject Label(GameObject canvas, string fontName, string text, float width, float height, float left, float top,
        float fontHeight, TextAnchor align, Color color)
    {
        float screenWidth = Screen.width;
        float screenHeight = Screen.height;

        float Width = (width * screenWidth) / 1000f;
        float Height = (height * screenHeight) / 1000f;
        float Left = (left * screenWidth) / 1000f;
        float Top = ((1000 - top - height) * screenHeight) / 1000f;

        GameObject newLabel = new GameObject();
        newLabel.AddComponent<Text>();
        newLabel.transform.SetParent(canvas.transform);

        RectTransform objectRectTransform = newLabel.GetComponent<RectTransform>();
        Vector2 position = Vector2.zero;
        position.x = Left + Width / 2;
        position.y = Top + Height / 2;
        objectRectTransform.transform.position = position;

        objectRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Width);
        objectRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Height);

        newLabel.GetComponent<Text>().text = text;
        newLabel.GetComponent<Text>().font = UIResources.getFont(fontName);
        newLabel.GetComponent<Text>().alignment = align;
        newLabel.GetComponent<Text>().color = color;
        newLabel.GetComponent<Text>().fontSize = (int)((screenHeight * fontHeight) / 1000f);

        return newLabel;
    }

    public static GameObject Label(GameObject canvas, string fontName, string text, float width, float height, float left, float top,
        float fontHeight, TextAnchor align, Color color, bool bestFit)
    {
        GameObject newLabel = Label(canvas, fontName, text, width, height, left, top, fontHeight, align, color);
        newLabel.GetComponent<Text>().resizeTextForBestFit = bestFit;
        newLabel.GetComponent<Text>().resizeTextMaxSize = (int)Positioner.getScaledY(fontHeight);
        newLabel.GetComponent<Text>().resizeTextMinSize = 1;

        return newLabel;
    }

    public static void setTouchable(GameObject gameObject, bool enabled)
    {
        GraphicRaycaster rayCaster = gameObject.GetComponent<GraphicRaycaster>();
        if (rayCaster == null)
        {
            rayCaster = gameObject.AddComponent<GraphicRaycaster>();
        }
        rayCaster.enabled = enabled;
    }

    public static void setImageVisible(GameObject obj, bool visible)
    {
        //obj.SetActive(visible);
        obj.GetComponent<Image>().enabled = visible;
    }

    public static void setText(GameObject canvas, string text)
    {
        canvas.GetComponent<Text>().text = text;
    }

    public static void setPosition(GameObject obj, float x, float y)
    {
        RectTransform transform = obj.GetComponent<RectTransform>();

        float screenWidth = Screen.width;
        float screenHeight = Screen.height;

        float Left = (x * screenWidth) / 1000f;
        float Top = ((1000 - y) * screenHeight) / 1000f;

        Vector2 position = Vector2.zero;
        position.x = Left + transform.rect.width / 2;
        position.y = Top - transform.rect.height / 2;
        transform.position = position;
    }

    public static void setPositionNonScaled(GameObject obj, float x, float y)
    {
        RectTransform transform = obj.GetComponent<RectTransform>();

        Vector2 position = new Vector2(x, y);
        transform.position = position;
    }

    public static GameObject Position(GameObject canvas, float width, float height, float left, float top)
    {
        float screenWidth = Screen.width;
        float screenHeight = Screen.height;

        float Width = (width * screenWidth) / 1000f;
        float Height = (height * screenHeight) / 1000f;
        float Left = (left * screenWidth) / 1000f;
        float Top = ((1000 - top - height) * screenHeight) / 1000f;

        RectTransform objectRectTransform = canvas.GetComponent<RectTransform>();
        Vector2 position = Vector2.zero;
        position.x = Left + Width / 2;
        position.y = Top + Height / 2;
        objectRectTransform.transform.position = position;
        objectRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Width);
        objectRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Height);
        //objectRectTransform.transform.localScale = new Vector2(Width / objectRectTransform.rect.width, Height / objectRectTransform.rect.height);
        return canvas;
    }

    public static GameObject PrefabPosition(GameObject canvas, float width, float height, float left, float top)
    {
        float screenWidth = Screen.width;
        float screenHeight = Screen.height;

        float Width = (width * screenWidth) / 1000f;
        float Height = (height * screenHeight) / 1000f;
        float Left = (left * screenWidth) / 1000f;
        float Top = ((1000 - top - height) * screenHeight) / 1000f;

        RectTransform objectRectTransform = canvas.GetComponent<RectTransform>();
        Vector2 position = Vector2.zero;
        position.x = Left + Width / 2;
        position.y = Top + Height / 2;
        objectRectTransform.transform.position = position;
        //objectRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Width);
        //objectRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Height);
        objectRectTransform.transform.localScale = new Vector2(Width / objectRectTransform.rect.width, Height / objectRectTransform.rect.height);
        return canvas;
    }

    public static GameObject setPositionLabel(GameObject canvas, float width, float height, float left, float top)
    {
        float screenWidth = Screen.width;
        float screenHeight = Screen.height;

        float Width = (width * screenWidth) / 1000f;
        float Height = (height * screenHeight) / 1000f;
        float Left = (left * screenWidth) / 1000f;
        float Top = ((1000 - top - height) * screenHeight) / 1000f;

        RectTransform objectRectTransform = canvas.GetComponent<RectTransform>();
        Vector2 position = Vector2.zero;
        position.x = Left + Width / 2;
        position.y = Top + Height / 2;
        objectRectTransform.transform.position = position;

        objectRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Width);
        objectRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Height);
        return canvas;
    }


    public static void setImage(GameObject canvas, Sprite sprite)
    {
        canvas.GetComponent<Image>().sprite = sprite;
    }

    public static AudioSource Sound(GameObject canvas, string soundName, float volume, bool loop)
    {
        AudioSource source = canvas.AddComponent<AudioSource>();
        source.clip = UIResources.getClip(soundName);
        source.playOnAwake = false;
        source.loop = loop;
        source.volume = volume;
        return source;
    }

    public static void playPartical(GameObject obj, float z)
    {
        setZ(obj, z);
        obj.GetComponent<ParticleSystem>().Play();
    }

    public static void setZ(GameObject obj, float z)
    {
        Vector3 position = obj.transform.position;
        position.z = z;
        obj.transform.position = position;
    }

    public static GameObject Sprite(GameObject canvas, Sprite resource, float width, float height, float left, float top)
    {
        float screenWidth = Screen.width;
        float screenHeight = Screen.height;

        float Width = (width * screenWidth) / 1000f;
        float Height = (height * screenHeight) / 1000f;
        float Left = (left * screenWidth) / 1000f;
        float Top = ((1000 - top - height) * screenHeight) / 1000f;
        float imageWidth = resource.rect.width / 100f;
        float imageHeight = resource.rect.height / 100f;

        GameObject newImage = new GameObject();
        newImage.AddComponent<SpriteRenderer>();
        newImage.transform.SetParent(canvas.transform);

        Transform objectRectTransform = newImage.GetComponent<Transform>();
        Vector2 position = Vector2.zero;
        position.x = Left + Width / 2;
        position.y = Top + Height / 2;
        objectRectTransform.transform.position = position;

        //objectRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, imageWidth);
        //objectRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, imageHeight);
        objectRectTransform.transform.localScale = new Vector2(Width / imageWidth, Height / imageHeight);

        newImage.GetComponent<SpriteRenderer>().sprite = resource;
        newImage.AddComponent<CanvasRenderer>();

        return newImage;
    }

    public static float getTextWidth(GameObject text)
    {
        return getPixelToX(text.GetComponent<Text>().preferredWidth);
    }

}

