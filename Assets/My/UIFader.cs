﻿using UnityEngine;
using System;

public class UIFader : MonoBehaviour
{
    float fadeDelay;
    int direction = 0;
    CanvasGroup canvasGroup;
    Delegate delegateAction = null, noParameterDelegate = null;
    int parameter;

    void checkCanvasGroup()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        if (canvasGroup == null)
        {
            canvasGroup = gameObject.AddComponent<CanvasGroup>();
        }
    }

    public void fadeIn(float duration)
    {
        checkCanvasGroup();
        canvasGroup.alpha = 0f;
        direction = 1;
        fadeDelay = duration;
    }

    public void fadeOut(float duration)
    {
        checkCanvasGroup();
        canvasGroup.alpha = 1f;
        direction = -1;
        fadeDelay = duration;
    }

    public void fadeIn(float duration, Action<GameObject, int> action, int parameter)
    {
        delegateAction = action;
        this.parameter = parameter;
        fadeIn(duration);
    }

    public void fadeOut(float duration, Action<GameObject, int> action, int parameter)
    {
        delegateAction = action;
        this.parameter = parameter;
        fadeOut(duration);
    }

    void Update()
    {
        if (direction == 1)
        {
            canvasGroup.alpha += Time.deltaTime / fadeDelay;
            if (canvasGroup.alpha >= 1f)
            {
                canvasGroup.alpha = 1f;
                direction = 0;
                if (delegateAction != null)
                {
                    delegateAction.DynamicInvoke(gameObject, parameter);
                }
                if (noParameterDelegate != null)
                {
                    noParameterDelegate.DynamicInvoke();
                }
                Destroy(this);
            }
        }
        else if (direction == -1)
        {
            canvasGroup.alpha -= Time.deltaTime / fadeDelay;
            if (canvasGroup.alpha <= 0f)
            {
                canvasGroup.alpha = 0f;
                direction = 0;
                if (delegateAction != null)
                {
                    delegateAction.DynamicInvoke(gameObject, parameter);
                }
                if (noParameterDelegate != null)
                {
                    noParameterDelegate.DynamicInvoke();
                }
                Destroy(this);
            }
        }
        else
        {
            Destroy(this);
        }
    }

    public static void fadeIn(GameObject obj, float duration)
    {
        UIFader fader = obj.AddComponent<UIFader>();
        fader.checkCanvasGroup();
        fader.canvasGroup.alpha = 0f;
        fader.direction = 1;
        fader.fadeDelay = duration;
    }

    public static void fadeOut(GameObject obj, float duration)
    {
        UIFader fader = obj.AddComponent<UIFader>();
        fader.checkCanvasGroup();
        fader.canvasGroup.alpha = 1f;
        fader.direction = -1;
        fader.fadeDelay = duration;
    }

    public static void fadeIn(GameObject obj, float duration, Action<GameObject, int> action, int parameter)
    {
        UIFader fader = obj.AddComponent<UIFader>();
        fader.delegateAction = action;
        fader.parameter = parameter;
        fader.fadeIn(duration);
    }

    public static void fadeOut(GameObject obj, float duration, Action<GameObject, int> action, int parameter)
    {
        UIFader fader = obj.AddComponent<UIFader>();
        fader.delegateAction = action;
        fader.parameter = parameter;
        fader.fadeOut(duration);
    }

    public static void fadeIn(GameObject obj, float duration, Action action)
    {
        UIFader fader = obj.AddComponent<UIFader>();
        fader.noParameterDelegate = action;
        fader.fadeIn(duration);
    }

    public static void fadeOut(GameObject obj, float duration, Action action)
    {
        UIFader fader = obj.AddComponent<UIFader>();
        fader.noParameterDelegate = action;
        fader.fadeOut(duration);
    }

    public static bool isFading(GameObject obj)
    {
        return (obj.GetComponent<UIFader>() != null);
    }

}
