﻿using UnityEngine;
using System;

public class UIMover : MonoBehaviour
{
    float xDiff, yDiff, moveTime, currentTime;
    float startX, startY, currentX, currentY;
    Delegate deletageAction = null;
    int parameter;
    InterpolationMain interpolation = Interpolation.linear;

    void Update()
    {
        if (moveTime > 0f)
        {
            currentTime += Time.deltaTime;
            if (currentTime >= moveTime)
            {
                currentTime = moveTime;
            }
            Vector3 position = transform.position;
            position.x = startX + (xDiff * interpolation.apply(currentTime / moveTime));
            position.y = startY + (yDiff * interpolation.apply(currentTime / moveTime));
            transform.position = position;
            if (currentTime >= moveTime)
            {
                moveTime = 0f;
                if (deletageAction != null)
                {
                    deletageAction.DynamicInvoke(gameObject, parameter);
                }
            }
        }
        else
        {
            Destroy(this);
        }
    }

    public void startMove(float toX, float toY, float duration)
    {
        startX = transform.position.x;
        startY = transform.position.y;
        xDiff = toX - startX;
        yDiff = toY - startY;
        moveTime = duration;
        currentTime = 0f;
    }

    public void startMove(float toX, float toY, float duration, Action<GameObject,int> action, int parameter)
    {
        startMove(toX, toY, duration);
        deletageAction = action;
        this.parameter = parameter;
    }

    public static void startMoveNonScaled(GameObject obj, float toX, float toY, float duration)
    {
        UIMover mover = obj.AddComponent<UIMover>();
        mover.startX = mover.transform.position.x;
        mover.startY = mover.transform.position.y;
        mover.xDiff = toX - mover.startX;
        mover.yDiff = toY - mover.startY;
        mover.moveTime = duration;
        mover.currentTime = 0f;
    }

    public static void startMoveNonScaled(GameObject obj, float toX, float toY, float duration, Action<GameObject, int> action, int parameter)
    {
        UIMover mover = obj.AddComponent<UIMover>();
        mover.startMove(toX, toY, duration);
        mover.deletageAction = action;
        mover.parameter = parameter;
    }

    public static void startMoveNonScaled(GameObject obj, float toX, float toY, float duration, Action<GameObject, int> action, int parameter, InterpolationMain interpolation)
    {
        UIMover mover = obj.AddComponent<UIMover>();
        mover.deletageAction = action;
        mover.parameter = parameter;
        mover.interpolation = interpolation;
        mover.startMove(toX, toY, duration);
    }

    public static void startMove(GameObject obj, float toX, float toY, float duration, Action<GameObject, int> action, int parameter)
    {
        RectTransform transform = obj.GetComponent<RectTransform>();

        float screenWidth = Screen.width;
        float screenHeight = Screen.height;

        float Left = (toX * screenWidth) / 1000f;
        float Top = ((1000 - toY) * screenHeight) / 1000f;

        UIMover mover = obj.AddComponent<UIMover>();
        mover.deletageAction = action;
        mover.parameter = parameter;
        mover.startMove(Left + transform.rect.width / 2, Top - transform.rect.height / 2, duration);
    }

    public static void startMove(GameObject obj, float toX, float toY, float duration, Action<GameObject, int> action, int parameter, InterpolationMain interpolation)
    {
        RectTransform transform = obj.GetComponent<RectTransform>();

        float screenWidth = Screen.width;
        float screenHeight = Screen.height;

        float Left = (toX * screenWidth) / 1000f;
        float Top = ((1000 - toY) * screenHeight) / 1000f;

        UIMover mover = obj.AddComponent<UIMover>();
        mover.deletageAction = action;
        mover.parameter = parameter;
        mover.interpolation = interpolation;
        mover.startMove(Left + transform.rect.width / 2, Top - transform.rect.height / 2, duration);
    }


    public void stopMove()
    {
        moveTime = 0f;
    }
}
