﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIClick : EventTrigger
{
    Delegate delegateAction, delegateOnDragStart, delegateOnDragEnd, noParameterDelegate;
    int parameter;
    float startX, startY;

    public void addClickEvent(Action<GameObject, int> action, int parameter)
    {
        delegateAction = action;
        this.parameter = parameter;
    }

    public void addClickEvent(Action action)
    {
        noParameterDelegate = action;
    }

    public void addDragEvents(Action<GameObject,int> dragStartAction, Action<int> dragEndAction, int parameter)
    {
        delegateOnDragStart = dragStartAction;
        delegateOnDragEnd = dragEndAction;
        this.parameter = parameter;
    }

    private bool dragging;
    public bool dragEnabled = false;

    public void Update()
    {
        if (dragging)
        {
            transform.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        if (dragEnabled)
        {
            startX = transform.position.x;
            startY = transform.position.y;
            if (delegateOnDragStart != null)
            {
                delegateOnDragStart.DynamicInvoke(gameObject, parameter);
            }
            dragging = true;
        }
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        dragging = false;
        if (dragEnabled)
        {
            if (delegateOnDragEnd != null)
            {
                delegateOnDragEnd.DynamicInvoke(gameObject, parameter);
            }
        }
        else
        {
            if (delegateAction != null)
            {
                delegateAction.DynamicInvoke(gameObject, parameter);
            }
            if (noParameterDelegate != null)
            {
                noParameterDelegate.DynamicInvoke();
            }
        }
    }

    public static void setDragEnabled(GameObject obj, bool enabled)
    {
        UIClick clicker = obj.GetComponent<UIClick>();
        if (clicker == null)
        {
            clicker = obj.AddComponent<UIClick>();
        }
        clicker.dragEnabled = enabled;
    }

    public static void addDragEvents(GameObject obj, Action<GameObject,int> dragStartAction, Action<GameObject,int> dragEndAction, int parameter)
    {
        UIClick clicker = obj.GetComponent<UIClick>();
        if (clicker == null)
        {
            clicker = obj.AddComponent<UIClick>();
        }
        clicker.delegateOnDragStart = dragStartAction;
        clicker.delegateOnDragEnd = dragEndAction;
        clicker.parameter = parameter;
        clicker.dragEnabled = true;
    }

    public static void moveBackToPositionBeforeDragging(GameObject obj)
    {
        UIClick clicker = obj.GetComponent<UIClick>();
        if (clicker != null)
        {
            Vector3 position = obj.transform.position;
            position.x = clicker.startX;
            position.y = clicker.startY;
            obj.transform.position = position;
        }
    }

    public static void moveBackToPositionBeforeDragging(GameObject obj, float duration)
    {
        UIClick clicker = obj.GetComponent<UIClick>();
        if (clicker != null)
        {
            UIMover.startMoveNonScaled(obj, clicker.startX, clicker.startY, duration);
        }
    }

}
