﻿using System;

public class Interpolation
{
    public static BounceOut bounceOut = new BounceOut(3);
    public static BounceIn bounceIn = new BounceIn(3);
    public static Linear linear = new Linear();
    public static Sine sine = new Sine();
    public static SineIn sineIn = new SineIn();
    public static SineOut sineOut = new SineOut();
};

public abstract class InterpolationMain
{
    abstract public float apply(float a);

    public float apply(float start, float end, float a)
    {
        return start + (end - start) * apply(a);
    }
};


public class BounceOut : InterpolationMain
{
    float[] widths, heights;

    public BounceOut(int bounces)
    {
        widths = new float[bounces];
        heights = new float[bounces];
        heights[0] = 1;
        switch (bounces)
        {
            case 2:
                widths[0] = 0.6f;
                widths[1] = 0.4f;
                heights[1] = 0.33f;
                break;
            case 3:
                widths[0] = 0.4f;
                widths[1] = 0.4f;
                widths[2] = 0.2f;
                heights[1] = 0.33f;
                heights[2] = 0.1f;
                break;
            case 4:
                widths[0] = 0.34f;
                widths[1] = 0.34f;
                widths[2] = 0.2f;
                widths[3] = 0.15f;
                heights[1] = 0.26f;
                heights[2] = 0.11f;
                heights[3] = 0.03f;
                break;
            case 5:
                widths[0] = 0.3f;
                widths[1] = 0.3f;
                widths[2] = 0.2f;
                widths[3] = 0.1f;
                widths[4] = 0.1f;
                heights[1] = 0.45f;
                heights[2] = 0.3f;
                heights[3] = 0.15f;
                heights[4] = 0.06f;
                break;
        }
        widths[0] *= 2;
    }

    override public float apply(float a)
    {
        a += widths[0] / 2;
        float width = 0, height = 0;
        for (int i = 0, n = widths.Length; i < n; i++)
        {
            width = widths[i];
            if (a <= width)
            {
                height = heights[i];
                break;
            }
            a -= width;
        }
        a /= width;
        float z = 4 / width * height * a;
        return 1 - (z - z * a) * width;
    }
}


public class Linear : InterpolationMain
{
    public Linear()
    {
    }

    override public float apply(float a)
    {
        return a;
    }
}

public class BounceIn : BounceOut
{
    public BounceIn(int bounce) : base(bounce)
    {        
    }

    public override float apply(float a)
    {
        return 1 - base.apply(1 - a);
    }
}

public class Sine : InterpolationMain
{
    public Sine()
    {
    }

    public override float apply(float a)
    {
        return (float)(1 - Math.Cos(a * Math.PI)) / 2;
    }
}

public class SineIn : InterpolationMain
{
    public SineIn()
    {
    }

    public override float apply(float a)
    {
        return (float)(1 - Math.Cos(a * Math.PI / 2));
    }
}

public class SineOut : InterpolationMain
{
    public SineOut()
    {
    }

    public override float apply(float a)
    {
        return (float)(Math.Sin(a * Math.PI / 2));
    }
}

