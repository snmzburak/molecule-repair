﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Init : MonoBehaviour
{

    void Start()
    {
        UIResources.loadResources();
        Game.init(gameObject);
    }

}
