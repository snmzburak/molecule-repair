﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BoardClick : EventTrigger
{
    public override void OnPointerUp(PointerEventData eventData)
    {
        int col = (int)((Positioner.getPixelToX(eventData.position.x) - Game.boardLeft) / Game.itemWidth);
        int row = (int)((Positioner.getPixelToY(eventData.position.y) - Game.boardTop) / Game.itemHeight);
        Game.doSelection(row, col);
    }



}
