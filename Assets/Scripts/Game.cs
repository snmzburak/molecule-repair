﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{

    public static GameObject gameGroup;
    public static int NUMBER_OF_ROWS = 7;
    public static int NUMBER_OF_COLS = 7;
    public static float boardLeft, boardTop, itemWidth, itemHeight, boardWidth, boardHeight;
    public static float solutionBoardLeft, solutionBoardTop, solutionBoardItemWidth, solutionBoardItemHeight, 
        solutionBoardHeight, solutionBoardWidth;

    public static GameObject[] atoms = new GameObject[NUMBER_OF_COLS * NUMBER_OF_ROWS];
    public static int[,] boardAtomIDs = new int[NUMBER_OF_COLS, NUMBER_OF_ROWS];
    public static int[] atomRow = new int[NUMBER_OF_COLS * NUMBER_OF_ROWS];
    public static int[] atomCol = new int[NUMBER_OF_COLS * NUMBER_OF_ROWS];

    public static GameObject selectionBox, solutionBoard, moleculeName;
    public static int selectRow = -1, selectCol = -1, hideRow, hideCol;
    public static bool canSelect = true;
    public static int[] atomIDs = new int[NUMBER_OF_COLS * NUMBER_OF_ROWS];
    public static int atomCounter = 1;
    public static int currentLevel = 0;

    public static void init(GameObject group)
    {
        gameGroup = Positioner.Image(group, UIResources.getSprite("empty"), 1000, 1000, 0, 0);
        itemWidth = 900f / NUMBER_OF_COLS;
        itemHeight = itemWidth / Positioner.getScaledYX();
        while (itemHeight > 90f)
        {
            itemWidth--;
            itemHeight = itemWidth / Positioner.getScaledYX();
        }
        boardLeft = (1000 - itemWidth * NUMBER_OF_COLS) / 2;
        boardTop = (950f - itemHeight * NUMBER_OF_ROWS);
        boardWidth = itemWidth * NUMBER_OF_COLS;
        boardHeight = itemHeight * NUMBER_OF_ROWS;


        float height = 1000f;
        float width = height * Positioner.getScaledYX();
        Positioner.Image(gameGroup, UIResources.getSprite("background"), width, height, 500-width/2, 0);
        GameObject board = Positioner.Image(gameGroup, UIResources.getSprite("board"), itemWidth * NUMBER_OF_COLS, itemHeight * NUMBER_OF_ROWS, boardLeft, boardTop);
        board.AddComponent<BoardClick>();

        float diff = 0.05f;
        GameObject boardFrame = Positioner.Image(gameGroup, UIResources.getSprite("boardframe"), boardWidth * (1f + diff*2),
            boardHeight * (1f + diff*2), boardLeft - boardWidth * diff, boardTop - boardHeight * diff);
        Positioner.setTouchable(boardFrame, false);

        moleculeName = Positioner.Label(gameGroup, "abaku", "", 800, 50, 100, 0, 40, TextAnchor.MiddleCenter, new Color(1f,190f/256f,0f),true);
        selectionBox = Positioner.Image(gameGroup, UIResources.getSprite("select"), itemWidth, itemHeight, 0, 0);
        Positioner.setTouchable(selectionBox, false);
        selectionBox.SetActive(false);
        loadLevel(currentLevel);
    }

    public static void resizeSolutionBoard(int level)
    {
        float diff = 0.05f;
        Puzzler.calculatePuzzleSize(level);
        solutionBoardTop = 50f;
        solutionBoardHeight = boardTop - solutionBoardTop - 20f - boardHeight*diff;

        solutionBoardItemWidth = itemWidth;
        solutionBoardItemHeight = itemHeight;

        while(solutionBoardItemHeight*Puzzler.puzzleSizeY > solutionBoardHeight)
        {
            solutionBoardItemWidth--;
            solutionBoardItemHeight = solutionBoardItemWidth / Positioner.getScaledYX();
        }


        solutionBoardWidth = solutionBoardItemWidth * Puzzler.puzzleSizeX;
        solutionBoardHeight = solutionBoardItemHeight * Puzzler.puzzleSizeY;
        solutionBoardLeft = 500f - solutionBoardWidth / 2;

        if (solutionBoard != null)
        {
            Destroy(solutionBoard);
        }
        solutionBoard = Positioner.Image(gameGroup, UIResources.getSprite("frame"), solutionBoardWidth, solutionBoardHeight,
            solutionBoardLeft, solutionBoardTop, () => { resetLevel(); });

    }

    public static void loadLevel(int level)
    {
        atomCounter = 1;
        for (int count = 0; count < atoms.Length; count++)
        {
            if (atoms[count] != null)
            {
                Destroy(atoms[count]);
            }
        }

        resizeSolutionBoard(level);

        for (int count = 0; count < NUMBER_OF_COLS * NUMBER_OF_ROWS; count++)
        {
            if (Puzzler.molecules[level, count] != 0)
            {
                int whichAtom = (Puzzler.molecules[level, count] & 0x0F00) / 256;
                GameObject solutionAtom = Positioner.Image(solutionBoard, UIResources.getSprite("atom"+whichAtom.ToString()),
                    solutionBoardItemWidth, solutionBoardItemHeight,
                    solutionBoardLeft + ((count % NUMBER_OF_COLS) - Puzzler.puzzleStartCol) * solutionBoardItemWidth,
                    solutionBoardTop + ((count / NUMBER_OF_COLS) - Puzzler.puzzleStartRow) * solutionBoardItemHeight);
                createConnections(solutionAtom, Puzzler.molecules[level, count], count, true);
                Positioner.setTouchable(solutionAtom, false);
            }

            if (Puzzler.levels[level, count] != 0)
            {
                int whichAtom = (Puzzler.levels[level, count] & 0x0F00) / 256;
                atoms[atomCounter] = Positioner.Image(gameGroup, UIResources.getSprite("atom"+whichAtom.ToString()), itemWidth, itemHeight,
                    boardLeft + (count % NUMBER_OF_COLS) * itemWidth, boardTop + (count / NUMBER_OF_COLS) * itemHeight);
                createConnections(atoms[atomCounter], Puzzler.levels[level, count], count, false);
                Positioner.setTouchable(atoms[atomCounter], false);
                atomIDs[atomCounter] = Puzzler.levels[level, count];
                boardAtomIDs[count / NUMBER_OF_COLS, count % NUMBER_OF_COLS] = atomCounter++;
            }
            else
            {
                boardAtomIDs[count / NUMBER_OF_COLS, count % NUMBER_OF_COLS] = 0;
            }
        }
        Positioner.setText(moleculeName, Puzzler.moleculeNames[level]);
    }

    public static void resetLevel()
    {
        loadLevel(currentLevel);
    }

    public static int boardStartCol, boardStartRow;
    public static void calculateBoardSize()
    {
        int minCol = Game.NUMBER_OF_COLS * 2, minRow = Game.NUMBER_OF_ROWS * 2;

        for (int row = 0; row < Game.NUMBER_OF_ROWS; row++)
        {
            for (int col = 0; col < Game.NUMBER_OF_COLS; col++)
            {
                if (boardAtomIDs[row,col] != 0)
                {
                    if (col < minCol) minCol = col;
                    if (row < minRow) minRow = row;
                }
            }
        }
        boardStartCol = minCol;
        boardStartRow = minRow;
    }


    public static void checkSolution()
    {
        calculateBoardSize();
        bool okay = true;
        for (int row = boardStartRow; row < NUMBER_OF_ROWS; row++)
        {
            for (int col = boardStartCol; col < NUMBER_OF_COLS; col++)
            {
                int count = (row - boardStartRow + Puzzler.puzzleStartRow) * NUMBER_OF_COLS + (col - boardStartCol + Puzzler.puzzleStartCol);
                if (count >= NUMBER_OF_COLS*NUMBER_OF_ROWS)
                {
                    okay = false;
                }
                if (okay)
                {
                    if (atomIDs[boardAtomIDs[row, col]] != Puzzler.molecules[currentLevel, count])
                    {
                        okay = false;
                    }
                }
            }
        }
        if (okay)
        {
            currentLevel++;
            loadLevel(currentLevel);
        }
    }

    public static void createConnections(GameObject atomObject, int atom, int count, bool solution)
    {
        int multiplier = 0x01;
        for (int shift = 0; shift < 8; shift++)
        {
            if ((atom & multiplier) != 0)
            {
                if (solution)
                {
                    Positioner.Image(atomObject, UIResources.getSprite((shift + 1).ToString()), solutionBoardItemWidth, solutionBoardItemHeight,
                        solutionBoardLeft + ((count % NUMBER_OF_COLS) - Puzzler.puzzleStartCol) * solutionBoardItemWidth,
                        solutionBoardTop + ((count / NUMBER_OF_COLS) - Puzzler.puzzleStartRow) * solutionBoardItemHeight);
                }
                else
                {
                    Positioner.Image(atomObject, UIResources.getSprite((shift + 1).ToString()), itemWidth, itemHeight,
                        boardLeft + (count % NUMBER_OF_COLS) * itemWidth, boardTop + (count / NUMBER_OF_COLS) * itemHeight);
                }
            }
            multiplier *= 2;
        }
    }


    public static void doSelection(int row, int col)
    {
        if (canSelect)
        {
            if ((selectRow == -1) && (selectCol == -1))
            {
                if (boardAtomIDs[row, col] != 0)
                {
                    selectRow = row;
                    selectCol = col;
                    Positioner.setPosition(selectionBox, boardLeft + col * itemWidth, boardTop + row * itemHeight);
                    selectionBox.SetActive(true);
                }
            }
            else if ((boardAtomIDs[row, col] != 0))
            {
                selectRow = row;
                selectCol = col;
                Positioner.setPosition(selectionBox, boardLeft + col * itemWidth, boardTop + row * itemHeight);
                selectionBox.SetActive(true);
            }
            else
            {
                if (doMove(selectRow, selectCol, row, col))
                {
                    canSelect = false;
                    selectRow = -1;
                    selectCol = -1;
                    selectionBox.SetActive(false);
                    atoms[boardAtomIDs[row, col]].transform.SetAsLastSibling();
                    UIMover.startMove(atoms[boardAtomIDs[row, col]], boardLeft + col * itemWidth, boardTop + row * itemHeight, 0.5f,
                        (obj, param) =>
                        {
                            canSelect = true;
                            int whichAtom = boardAtomIDs[hideRow, hideCol];
                            boardAtomIDs[hideRow, hideCol] = 0;
                            UIFader.fadeOut(atoms[whichAtom], 0.5f, () =>
                            {
                                Destroy(atoms[boardAtomIDs[hideRow, hideCol]]);
                                checkSolution();
                            });
                        }, 0);
                }
                else
                {
                    selectRow = -1;
                    selectCol = -1;
                    selectionBox.SetActive(false);
                }
            }
        }
    }

    public static bool doMove(int row1, int col1, int row2, int col2)
    {
        if ((row1 == row2) && (col1 == col2 + 2))
        {
            if (boardAtomIDs[row2, col2 + 1] > 0)
            {
                boardAtomIDs[row2, col2] = boardAtomIDs[row1, col1];
                boardAtomIDs[row1, col1] = 0;
                hideRow = row1;
                hideCol = col2 + 1;
                return true;
            }
        }
        else if ((row1 == row2) && (col1 == col2 - 2))
        {
            if (boardAtomIDs[row2, col2 - 1] > 0)
            {
                boardAtomIDs[row2, col2] = boardAtomIDs[row1, col1];
                boardAtomIDs[row1, col1] = 0;
                hideRow = row1;
                hideCol = col2 - 1;
                return true;
            }
        }
        else if ((col1 == col2) && (row1 == row2 + 2))
        {
            if (boardAtomIDs[row2 + 1, col2] > 0)
            {
                boardAtomIDs[row2, col2] = boardAtomIDs[row1, col1];
                boardAtomIDs[row1, col1] = 0;
                hideRow = row2 + 1;
                hideCol = col2;
                return true;
            }
        }
        else if ((col1 == col2) && (row1 == row2 - 2))
        {
            if (boardAtomIDs[row2 - 1, col2] > 0)
            {
                boardAtomIDs[row2, col2] = boardAtomIDs[row1, col1];
                boardAtomIDs[row1, col1] = 0;
                hideRow = row2 - 1;
                hideCol = col2;
                return true;
            }
        }
        return false;
    }
}